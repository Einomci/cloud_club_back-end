from django.urls import path

from my_app.views import AbstractUser, Activity, Club, Push, Bill, Comment

urlpatterns = [
    # user
    path('register/', AbstractUser.Register.as_view(), name='register'),  # 用户注册
    path('login/', AbstractUser.Login.as_view(), name='login'),  # 用户登录
    path('logout/', AbstractUser.Logout.as_view(), name='logout'),  # 用户登出
    path('user_profile/', AbstractUser.UserProfile.as_view(), name='user_profile'),  # 获取用户信息
    path('upload_profile/', AbstractUser.UploadProfile.as_view(), name='upload_profile'),  # 上传用户头像
    path('change_password/', AbstractUser.ChangePassword.as_view(), name='change_password'),  # 修改密码
    # activity
    path('create_activity/', Activity.CreateActivity.as_view(), name='create_activity'),  # 创建活动
    path('get_create_activity/', Activity.GetActivityRequests.as_view(), name='get_activity_requests'),  # 获取创建请求列表
    path('exam_create_activity/', Activity.ExamActivity.as_view(), name='exam_create_activity'),  # 审批创建请求

    path('browse_activity/', Activity.BrowseActivity.as_view(), name='browse_activity'),  # 拉取活动列表
    path('get_activity_profile/', Activity.GetActivityProfile.as_view(), name='get_activity_profile'),  # 获取活动详情
    path('attend_activity/', Activity.AttendActivity.as_view(), name='attend_activity'),  # 参加活动
    # club
    path('create_club/', Club.CreateClub.as_view(), name='create_club'),  # 创建社团
    path('get_create_club/', Club.GetCreateReqs.as_view(), name='get_create_requests'),  # 获取创建社团请求列表
    path('exam_create_club/', Club.ExamCreateClub.as_view(), name='exam_create_club'),  # 审核创建社团请求

    path('browse_club/', Club.BrowseClub.as_view(), name='browse_club'),  # 拉取社团列表
    path('get_club_profile/', Club.GetClubProfile.as_view(), name='get_club_profile'),  # 获取社团详情
    path('attend_club/', Club.AttendClub.as_view(), name='attend_club'),  # 参加社团
    # push
    path('make_push/', Push.MakePush.as_view(), name='make_push'),  # 创建推送
    path('browse_push/', Push.BrowsePush.as_view(), name='browse_push'),  # 拉取推送列表
    # bill
    path('pub_bill_request/', Bill.PubBillReq.as_view(), name='pub_bill_request'),  # 提交报销请求
    path('get_bill_requests/', Bill.GetBillReq.as_view(), name='get_bill_requests'),  # 获取报销请求列表
    path('exam_bill/', Bill.ExamBill.as_view(), name='exam_bill'),  # 审批报销请求
    # comment
    path('add_comment/<str:item_type>/', Comment.AddCommentToItem.as_view(), name='add_comment_to_item'),
    # 对项目进行评论，可以多次进行，不会覆盖 item_type可以取值为 'club' 'activity' 'push' 例如 add_comment/club/
    path('add_score/<str:item_type>/', Comment.AddScoreToItem.as_view(), name='add_comment_to_item'),
    # 对项目评分，可以多次进行，会覆盖之前的 item_type可以取值为 'club' 'activity' 'push'
    path('delete_comment/', Comment.DeleteComment.as_view(), name='delete_comment'),
    # 对项目评论进行删除
]
