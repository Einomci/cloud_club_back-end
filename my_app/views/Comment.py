from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from my_app.models import *


class AddCommentToItem(APIView):
    def post(self, request, item_type):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)

            item_id = request.data.get('id')
            item = None

            if item_type == 'club':
                item = Club.objects.get(id=item_id)
            elif item_type == 'activity':
                item = Activity.objects.get(id=item_id)
            elif item_type == 'push':
                item = Push.objects.get(id=item_id)

            text = request.data.get('text')

            comment = Comment.objects.create(
                text=text,
                student=student,
                relate_item=item
            )
            comment.save()

            return Response({"status": 200, "msg": "评论添加成功"})
        except Student.DoesNotExist:
            return Response({"status": 404, "msg": "学生不存在"})
        except ObjectDoesNotExist:
            return Response({"status": 404, "msg": f"{item_type.capitalize()}不存在"})
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class DeleteComment(APIView):
    def post(self, request):
        try:
            comment_id = request.data.get('id')
            comment = Comment.objects.get(id=comment_id)
            user = comment.student
            logged_in_user = request.session.get('username')

            if (
                    user.username == logged_in_user
                    or user.is_staff
            ):
                comment.delete()
                return Response({"status": 200, "msg": "评论删除成功"})
            else:
                return Response({"status": 403, "msg": "无权限删除此评论"})
        except Comment.DoesNotExist:
            return Response({"status": 404, "msg": "评论不存在"})
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class AddScoreToItem(APIView):
    def post(self, request, item_type):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)

            item_id = request.data.get('id')
            value = request.data.get('score')
            item = None

            if item_type == 'club':
                item = Club.objects.get(id=item_id)
            elif item_type == 'activity':
                item = Activity.objects.get(id=item_id)
            elif item_type == 'push':
                item = Push.objects.get(id=item_id)

            existing_score = Score.objects.filter(student=student, relate_item=item).first()
            if existing_score:
                existing_score.value = value
                existing_score.save()
            else:
                Score.objects.create(
                    value=value,
                    relate_item=item,
                    student=student,
                )

            all_scores = Score.objects.filter(relate_item=item)
            total_score = sum(score.value for score in all_scores)
            average_score = total_score / len(all_scores) if len(all_scores) > 0 else None

            item.average_score = average_score
            item.save()
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})
