import time
from datetime import datetime
from cloudClub.settings import *
from rest_framework.response import Response

from cloudClub import settings


def store_file(file, name):
    suffix = file.name.rsplit('.')[-1]
    fn = time.strftime('%Y-%m-%d-%H-%M-%S')
    image_name = name + "_" + fn + "." + suffix
    local_dir = settings.LOCAL_IMG_DIR + str(image_name)
    post_dir = settings.POST_IMG_PATH + str(image_name)
    destination = open(local_dir, 'wb+')
    for chunk in file.chunks():
        destination.write(chunk)
    destination.close()
    return [local_dir, post_dir]


def debug_club_profile():
    return Response({"status": 200, "data": {
        "club_id": 1,
        "name": "极限飞盘社",
        "logo_url": "http://8.130.117.60:8080/img/2.png",
        "star_comment": "这个社团真的很棒！",
        "average_score": 4.5,
        "star": 5,
        "category": "体育",
        "introduction": "欢迎加入我们的飞盘社，一起感受飞盘的魅力！",
        "attend": ATTEND_TRUE,
        "comments": [
            {
                "comment_id": 1,
                "text": "这个社团真的很有趣！",
                "student": {
                    "student_id": "user123",
                    "student_username": "John Doe",
                    "student_profile_url": "http://8.130.117.60:8080/img/1.jpg",
                }
            },
        ]
    }})


def debug_activity_requests():
    data = []
    for i in range(5):
        data.append({
            "activity_id": i * 123 + 456,
            "club_id": 1,
            "name": f"Activity {i + 1}",
            "start_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "end_time": (datetime.now()).strftime("%Y-%m-%d %H:%M:%S"),
            "content": f"Activity {i + 1} description",
            "area": f"Area {i + 1}",
            "status": STATUS_WAIT,
            "max_num": DEFAULT_ACTIVITY_MAXNUM,
            "image_url": "http://example.com/default.jpg",
        })
    return Response({"status": 200, "dtat": data})


def debug_activity_profile():
    return Response({"status": 200, "data": {
        "activity_id": 1,
        "club_id": 1,
        "image_url": "http://8.130.117.60:8080/img/3.jpg",
        "club_name": "极限飞盘社",
        "name": "飞盘嘉年华",
        "content": "同学们自己随便玩玩就行",
        "category": "体育",
        "area": "小足球场",
        "start_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "end_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "attend": ATTEND_FALSE,
        "comments": [
            {
                "comment_id": 1,
                "text": "这个活动真的很好玩！",
                "student": {
                    "student_id": "21373267",
                    "student_username": "武彬煦",
                    "student_profile_url": "http://8.130.117.60:8080/img/1.jpg",
                }
            },
        ]
    }})


def debug_brow_activity():
    data = []
    for i in range(10):
        data.append({
            "activity_id": i * 123 + 7,
            "club_id": 1,
            "image_url": "http://8.130.117.60:8080/img/123.jpg",
            "club_name": "极限飞盘社",
            "name": "飞盘嘉年华",
            "content": "同学们自己随便玩玩就行",
            "category": "体育",
            "area": "小足球场",
            "start_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "end_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "attend": "1"
        })
    return Response({"status": 200, "data": data})


def debug_bill_requests():
    data = []
    for i in range(5):
        data.append({
            "id": i * 423 + 567,
            "club_id": 1,
            "time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "money": 100 * (i + 1),
            "text": "报销请求说明",
            "status": STATUS_WAIT,
            "file_url": "http://8.130.117.60:8080/img/aaa.pdf",
        })
    return Response({"status": 200, "data": data})
