
from rest_framework.response import Response
from rest_framework.views import APIView, Request

from my_app.views.function import *
from my_app.models import *
from cloudClub.settings import *
from django.db import transaction


class CreateClub(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            name = request.data.get('name')
            category = request.data.get('category')
            introduction = request.data.get('introduction')
            profile_image = request.FILES.get('profile')
            [local_dir, post_url] = store_file(profile_image, name)
            # 创建请求对象并保存到数据库
            with transaction.atomic():
                profile = File.objects.create(local_dir=local_dir, post_url=post_url)  # 你需要提供实际的路径
                StudentCreateClub.objects.create(
                    student=student,
                    name=name,
                    category=category,
                    introduction=introduction,
                    profile=profile,
                    status=STATUS_WAIT,
                )
            return Response({"status": 200, "msg": "请求发送成功！请等待审核"})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class GetCreateReqs(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            status = request.data.get('status', 'ALL')  # 默认为ALL
            if not student.is_staff:
                return Response({"status": 400, "msg": "无权限审批活动"})
            pending_requests = StudentCreateClub.objects.all() if status == "ALL" else StudentCreateClub.objects.filter(
                status=status)

            serialized_data = {"requests": []}
            for request_item in pending_requests:
                serialized_data["requests"].append({
                    "id": request_item.id,
                    "name": request_item.name,
                    "category": request_item.category,
                    "introduction": request_item.introduction,
                    "profile_url": request_item.profile.post_url,
                    "request_date": request_item.pub_date.strftime("%Y-%m-%d"),
                    "status": request_item.status,
                })

            return Response({"status": 200, "msg": "获取申请列表", "data": serialized_data})

        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class ExamCreateClub(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            request_id = request.data.get('id')
            status = request.data.get('status')
            if not student.is_staff:
                return Response({"status": 400, "msg": "无权限审批活动"})

            create_request = StudentCreateClub.objects.get(id=request_id)
            if create_request.status != STATUS_WAIT:
                return Response({"status": 400, "msg": "请求已完成审批"})
            create_request.status = status
            create_request.save()
            if status == "AC":
                create_request.student.is_chairman = True
                create_request.student.save()
                new_club = Club.objects.create(
                    name=create_request.name,
                    category=create_request.category,
                    introduction=create_request.introduction,
                    profile=create_request.profile,
                    star=INIT_CLUB_STAR  # You can set a default value or calculate based on some criteria
                )
                new_club.save()

            return Response({"status": 200, "msg": "审批完成"})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except StudentCreateClub.DoesNotExist:
            return Response({"status": 400, "msg": "请求不存在"})  # 请求不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class BrowseClub(APIView):
    def post(self, request: Request):
        try:
            name = request.data.get('name', 'none')
            category = request.data.get('category', 'none')
            star = request.data.get('star', 'none')
            clubs = Club.objects.filter(
                name=name,
                category=category,
            )
            if star != 'none':
                clubs = clubs.filter(star__gte=star)
            data = []
            if DEBUG:
                data.append({
                    "club_id": 1,
                    "name": "club.name",
                    "category": "club.category",
                    "introduction": "club.introduction",
                    "profile_url": "http://8.130.117.60:8080/img/aaa.jpg",
                    "star": 3,
                })
            for club in clubs:
                data.append({
                    "club_id": club.id,
                    "name": club.name,
                    "category": club.category,
                    "introduction": club.introduction,
                    "profile_url": club.profile.post_url if club.profile else None,
                    "star": club.star,
                })
            return Response({"status": 200, "data": data})
        except Club.DoesNotExist:
            return Response({"status": 400, "msg": "社团不存在"})  # 社团不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class GetClubProfile(APIView):
    def post(self, request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            club_id = request.data.get('id')

            if DEBUG:
                return debug_club_profile()

            club = Club.objects.get(id=club_id)
            comments_data = []
            attend = ATTEND_FALSE
            if StudentAttendClub.objects.filter(student=student, club=club).count() != 0:
                attend = ATTEND_TRUE
            for comment in Comment.objects.filter(relate_item=club):
                student_data = {
                    "student_id": comment.student.username,
                    "student_username": comment.student.first_name,
                    "student_profile_url": comment.student.profile.post_url if comment.student.profile else None,
                }
                comment_data = {
                    "comment_id": comment.id,
                    "text": comment.text,
                    "student": student_data,
                }
                comments_data.append(comment_data)
            data = {
                "club_id": club.id,
                "name": club.name,
                "logo_url": club.profile.post_url if club.profile else None,
                "star_comment": club.star_comment.text if club.star_comment else None,
                "average_score": club.average_score,
                "star": club.star,
                "category": club.category,
                "introduction": club.introduction,
                "attend": attend,
                "comments": comments_data,
            }
            return Response({"status": 200, "data": data})
        except Club.DoesNotExist:
            return Response({"status": 400, "msg": "社团不存在"})  # 社团不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class AttendClub(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            club_id = request.data.get('id')
            club = Club.objects.get(id=club_id)

            StudentAttendClub.objects.create(
                student=student,
                club=club,
                status=STATUS_APPROVE,
            )

            return Response({"status": 200, "msg": "成功加入社团"})
        except Club.DoesNotExist:
            return Response({"status": 400, "msg": "社团不存在"})  # 社团不存在
        except Student.DoesNotExist:
            return Response({"status": 404, "msg": "学生不存在"})  # 学生不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误

