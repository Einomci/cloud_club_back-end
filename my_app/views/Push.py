from datetime import datetime

from cloudClub.settings import *
from my_app.models import *
from my_app.views.function import store_file
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.request import Request


class MakePush(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            chairman = Student.objects.get(username=student_id)
            if not chairman.is_chairman:
                return Response({"status": 400, "msg": "无权限创建"})  # 权限
            pub_time = datetime.now()
            name = request.data.get('name')
            url = request.data.get('url')
            text = request.data.get('text')
            profile_file = request.FILES.get('file')
            local_dir, post_url = store_file(profile_file, name)
            push = Push.objects.create(
                pub_time=pub_time,
                name=name,
                url=url,
                text=text,
                club=chairman.activity_club,
                profile=File.objects.create(local_dir=local_dir, post_url=post_url)
            )
            push.save()
            return Response({"status": 200, "msg": "推送创建成功"})
        except Club.DoesNotExist:
            return Response({"status": 400, "msg": "社团不存在"})  # 社团不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class BrowsePush(APIView):
    def post(self, request: Request):
        try:
            club_name = request.data.get('club', 'none')
            start_time_str = request.data.get('start_time', 'none')
            end_time_str = request.data.get('end_time', 'none')

            start_time = datetime.strptime(start_time_str, "%Y-%m-%d") if start_time_str != 'none' else None
            end_time = datetime.strptime(end_time_str, "%Y-%m-%d") if end_time_str != 'none' else None

            filter_dict = {
                key: value for key, value in {
                    'club__name__icontains': club_name if club_name != 'none' else None,
                    'pub_time__lte': end_time if end_time else None,
                    'pub_time__gte': start_time if start_time else None,
                }.items() if value is not None
            }

            pushes = Push.objects.filter(**filter_dict)
            data = []

            if DEBUG:
                for i in range(10):
                    data.append({
                        "id": i * 123 + 7,
                        "club_id": 1,
                        "pub_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        "name": "推送标题",
                        "url": "https://mp.weixin.qq.com/s/k6LJiJwQFiC09OCVDYH6EQ",
                        "text": "推送内容摘要",
                        "club_name": "社团名称",
                        "profile_url": "http://8.130.117.60:8080/img/2.png",
                    })
            for push in pushes:
                data.append({
                    "push_id": push.id,
                    "club_id": push.push_club.id,
                    "pub_time": push.pub_time.strftime("%Y-%m-%d %H:%M:%S"),
                    "name": push.name,
                    "url": push.url,
                    "text": push.text,
                    "club_name": push.activity_club.name,
                    "profile_url": push.profile.post_url if push.profile else None,
                })
            return Response({"status": 200, "data": data})
        except Push.DoesNotExist:
            return Response({"status": 400, "msg": "推送不存在"})  # 推送不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误
