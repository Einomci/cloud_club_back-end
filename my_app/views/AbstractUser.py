from rest_framework.response import Response
from rest_framework.views import APIView, Request
from django.contrib.auth.models import User
from django.contrib.auth import logout as dj_logout, login as dj_login
from my_app.models import Student, File
from cloudClub import settings
from my_app.views.function import store_file

AdminList = [21373267]


class Register(APIView):
    def post(self, request: Request):
        try:
            user_id = request.data.get('user_id')
            nickname = request.data.get('username')
            password = request.data.get('password')
            image = File.objects.create(
                post_url=settings.POST_IMG_DEFALUT + 'profile.jpg',
                local_dir=settings.LOCAL_IMG_DEFALUT + 'profile.jpg'
            )
            student = Student.objects.create_user(username=user_id, password=password, first_name=nickname,
                                                  profile=image, is_chairman=False)
            if int(user_id) in AdminList:
                student.is_staff = True  # 如果在 AdminList 中，将用户标记为管理员
            student.save()
            return Response({"status": 200, "msg": "注册成功"})  # 注册成功
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class Login(APIView):
    def post(self, request: Request):
        try:
            user_id = request.data.get('user_id')
            password = request.data.get('password')
            user = User.objects.get(username=user_id)

            if user.check_password(password):
                dj_login(request, user)
                request.session['username'] = user_id
                return Response({"status": 200, "msg": "登录成功"})  # 登录成功
            else:
                return Response({"status": 400, "msg": "用户名或密码错误"})  # 密码错误

        except User.DoesNotExist:
            return Response({"status": 400, "msg": "用户未注册"})  # 未注册
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误: {str(e)}"})  # 服务器错误


class Logout(APIView):
    def post(self, request: Request):
        try:
            user_id = request.session.get('username')
            if user_id:
                dj_logout(request)
                request.session.flush()
                return Response({"status": 200, "msg": "登出成功"})
            else:
                return Response({"status": 400, "msg": "用户未登录"})

        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误: {str(e)}"})


class UserProfile(APIView):
    def post(self, request: Request):
        try:
            user_id = request.session.get('username')
            student = Student.objects.get(username=user_id)
            profile_url = student.profile.post_url if student.profile else None

            user_profile_data = {
                "user_id": user_id,
                "email": student.email,
                "first_name": student.first_name,
                "profile_url": profile_url,
            }
            return Response({"status": 200, "data": user_profile_data})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误: {str(e)}"})  # 服务器错误


class UploadProfile(APIView):
    def post(self, request: Request):
        try:
            user_id = request.session.get('username')
            image_file = request.data.get('file')
            local_dir, post_url = store_file(image_file, user_id)
            student = Student.objects.get(username=user_id)
            image = File.objects.create(local_dir=local_dir, post_url=post_url)
            student.profile = image
            student.save()
            return Response({"status": 200, "msg": "成功上传"})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误: {str(e)}"})


class ChangePassword(APIView):
    def post(self, request: Request):
        try:
            user_id = request.session.get('username')
            old_password = request.data.get('old_pwd')
            new_password = request.data.get('new_pwd')
            new_password_again = request.data.get('new_pwd_again')
            if new_password_again != new_password:
                return Response({"status": 400, "msg": "两次新密码不一致"})  # 两次新密码不一致
            user = User.objects.get(username=user_id)
            if not user.check_password(old_password):
                return Response({"status": 400, "msg": "旧密码错误"})  # 旧密码错误

            user.set_password(new_password)
            user.save()
            return Response({"status": 200, "msg": "密码修改成功"})  # 密码修改成功

        except User.DoesNotExist:
            return Response({"status": 400, "msg": "用户不存在"})  # 用户不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误: {str(e)}"})  # 服务器错误
