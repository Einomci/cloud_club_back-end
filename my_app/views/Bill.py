from rest_framework.response import Response
from rest_framework.views import APIView, Request

from datetime import datetime, timezone
from cloudClub.settings import *
from my_app.views.function import *
from my_app.models import *


class PubBillReq(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            bill_file = request.FILES.get('file')
            student = Student.objects.get(username=student_id)

            if not student.is_chairman:
                return Response({"status": 400, "msg": "暂无权限"})
            time_str = request.data.get('time')
            bill_time = datetime.strptime(time_str, "%Y-%m-%d").replace(tzinfo=timezone.utc)

            local_dir, post_url = store_file(bill_file, student_id)
            bill = Bill.objects.create(
                time=bill_time,
                money=request.data.get('money'),
                text=request.data.get('text'),
                status=STATUS_WAIT,
                club=student.activity_club,
                file=File.objects.create(local_dir=local_dir, post_url=post_url)
            )
            bill.save()
            return Response({"status": 200, "msg": "报销请求成功，请等待审核"})  # 报销请求成功
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class GetBillReq(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            if not student.is_staff:
                return Response({"status": 400, "msg": "暂无权限"})  # 权限不足

            status = request.data.get('status', STATUS_ALL)
            bill_requests = Bill.objects.filter(status=status) if status != STATUS_ALL else Bill.objects.all()
            data = []
            if DEBUG:
                return debug_bill_requests()

            for bill_request in bill_requests:
                data.append({
                    "bill_id": bill_request.id,
                    "club_id": bill_request.club.id,
                    "time": bill_request.pub_date.strftime("%Y-%m-%d %H:%M:%S"),
                    "money": bill_request.money,
                    "text": bill_request.text,
                    "status": bill_request.status,
                    "file_url": bill_request.file.post_url if bill_request.file else None,
                })
            return Response({"status": 200, "data": data})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class ExamBill(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            if not student.is_staff:
                return Response({"status": 400, "msg": "无权限审批报销请求"})

            request_id = request.data.get('id')
            status = request.data.get('status')

            bill_request = Bill.objects.get(id=request_id)
            if bill_request.status != STATUS_WAIT:
                return Response({"status": 400, "msg": "请求已完成审批"})

            bill_request.status = status
            bill_request.save()

            return Response({"status": 200, "msg": "审批完成"})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Bill.DoesNotExist:
            return Response({"status": 400, "msg": "报销请求不存在"})  # 报销请求不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误
