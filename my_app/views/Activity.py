from datetime import datetime, timezone
from rest_framework.response import Response
from rest_framework.views import APIView, Request

from my_app.views.function import *
from my_app.models import *
from django.db import transaction
from my_app.views.function import store_file
from cloudClub.settings import *


class CreateActivity(APIView):
    def post(self, request: Request):
        try:
            user_id = request.session.get('username')
            student = Student.objects.get(username=user_id)
            if not student.is_chairman:
                return Response({"status": 400, "msg": "社长不存在"})

            name = request.data.get('name')
            start_time_str = request.data.get('start_time')
            end_time_str = request.data.get('end_time')
            content = request.data.get('content')
            area = request.data.get('area')
            image_file = request.FILES.get('image', )
            max_num = request.data.get('data', DEFAULT_ACTIVITY_MAXNUM)  # 活动最大人数
            start_time = datetime.strptime(start_time_str, "%Y-%m-%d").replace(tzinfo=timezone.utc)
            end_time = datetime.strptime(end_time_str, "%Y-%m-%d").replace(tzinfo=timezone.utc)

            [local_dir, post_dir] = store_file(image_file, name)
            # 创建活动对象并保存到数据库
            with transaction.atomic():
                image = File.objects.create(
                    local_dir=local_dir,
                    post_url=post_dir
                )
                StudentCreateActivity.objects.create(
                    name=name,
                    start_time=start_time,
                    end_time=end_time,
                    content=content,
                    image=image,
                    area=area,
                    club=student.activity_club,
                    maxNum=max_num,
                    status=STATUS_WAIT
                )
            return Response({"status": 200, "msg": "活动创建成功"})  # 活动创建成功
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class GetActivityRequests(APIView):
    def post(self, request: Request):
        try:
            user_id = request.session.get('username')
            student = Student.objects.get(username=user_id)

            if DEBUG:
                debug_activity_requests()

            if not student.is_staff:
                return Response({"status": 400, "msg": "暂无权限"})  # 权限不足

            status = request.data.get('status', STATUS_ALL)
            activity_requests = StudentCreateActivity.objects.filter(
                status=status) if status != STATUS_ALL else StudentCreateActivity.objects.all()
            data = []

            for activity_request in activity_requests:
                data.append({
                    "activity_id": activity_request.id,
                    "club_id": activity_request.activity_club.id,
                    "name": activity_request.name,
                    "start_time": activity_request.start_time.strftime("%Y-%m-%d %H:%M:%S"),
                    "end_time": activity_request.end_time.strftime("%Y-%m-%d %H:%M:%S"),
                    "content": activity_request.content,
                    "area": activity_request.area,
                    "status": activity_request.status,
                    "max_num": activity_request.maxNum,
                    "image_url": activity_request.profile.post_url if activity_request.profile else None,
                })
            return Response({"status": 200, "data": data})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class ExamActivity(APIView):
    def post(self, request: Request):
        try:
            user_id = request.session.get('username')
            student = Student.objects.get(username=user_id)
            if not student.is_staff:
                return Response({"status": 400, "msg": "无权限审批活动申请"})

            request_id = request.data.get('id')
            status = request.data.get('status')

            activity_request = StudentCreateActivity.objects.get(id=request_id)
            if activity_request.status != STATUS_WAIT:
                return Response({"status": 400, "msg": "申请已完成审批"})

            activity_request.status = status
            activity_request.save()

            activity = Activity.objects.create(
                activity_club=activity_request.activity_club,
                content=activity_request.content,
                area=activity_request.area,
                maxNum=activity_request.maxNum,
                start_time=activity_request.start_time,
                end_time=activity_request.end_time
            )
            activity.save()
            return Response({"status": 200, "msg": "审批完成"})
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except StudentCreateActivity.DoesNotExist:
            return Response({"status": 400, "msg": "活动申请不存在"})  # 活动申请不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class AttendActivity(APIView):
    def post(self, request: Request):
        try:
            student_id = request.session.get('username')
            activity_id = request.data.get('id')
            # 获取学生和活动对象
            student = Student.objects.get(username=student_id)
            activity = Activity.objects.get(id=activity_id)
            StudentAttendActivity.objects.create(
                status=STATUS_APPROVE,
                student=student,
                activity=activity,
            )
            return Response({"status": 200, "msg": "成功参加活动"})  # 成功参加活动
        except Student.DoesNotExist:
            return Response({"status": 400, "msg": "学生不存在"})  # 学生不存在
        except Activity.DoesNotExist:
            return Response({"status": 400, "msg": "活动不存在"})  # 活动不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class BrowseActivity(APIView):
    def post(self, request: Request):
        try:
            student_id = request.sessiong.get('username')
            student = Student.objects.get(username=student_id)
            club_name = request.data.get('club', 'none')
            category = request.data.get('category', 'none')
            area = request.data.get('area', 'none')
            start_time_str = request.data.get('start_time', 'none')
            end_time_str = request.data.get('end_time', 'none')
            attend = request.data.get('attend', '0')

            start_time = datetime.strptime(start_time_str, "%Y-%m-%d") if start_time_str != 'none' else None
            end_time = datetime.strptime(end_time_str, "%Y-%m-%d") if end_time_str != 'none' else None

            filter_dict = {
                key: value for key, value in {
                    'club__name__icontains': club_name if club_name != 'none' else None,
                    'category__icontains': category if category != 'none' else None,
                    'area__icontains': area if area != 'none' else None,
                    'start_time__lte': end_time if end_time else None,
                    'end_time__gte': start_time if start_time else None,
                }.items() if value is not None
            }

            activities = Activity.objects.filter(**filter_dict)
            if attend == ATTEND_TRUE:
                activities = activities.filter(attendees__username=request.session.get('username'))
            data = []

            if DEBUG:
                debug_brow_activity()
            for activity in activities:
                attend_item = ATTEND_FALSE
                if StudentAttendActivity.objects.filter(student=student, activity=activity).count() != 0:
                    attend_item = ATTEND_TRUE
                data.append({
                    "activity_id": activity.id,
                    "club_id": activity.activity_club.id,
                    "image_url": activity.profile.post_url if activity.profile else None,
                    "club_name": activity.activity_club.name,
                    "name": activity.name,
                    "content": activity.content,
                    "category": activity.activity_club.category,
                    "area": activity.area,
                    "start_time": activity.start_time.strftime("%Y-%m-%d %H:%M:%S"),
                    "end_time": activity.end_time.strftime("%Y-%m-%d %H:%M:%S"),
                    "attend": attend_item,
                })
            return Response({"status": 200, "data": data})
        except Activity.DoesNotExist:
            return Response({"status": 400, "msg": "活动不存在"})  # 活动不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误


class GetActivityProfile(APIView):
    def post(self, request):
        try:
            student_id = request.session.get('username')
            student = Student.objects.get(username=student_id)
            activity_id = request.data.get('id')

            # 添加 DEBUG 字段
            if DEBUG:
                return debug_activity_profile()

            activity = Activity.objects.get(id=activity_id)
            comments_data = []
            attend = ATTEND_FALSE
            if StudentAttendActivity.objects.filter(student=student, activity=activity).count() != 0:
                attend = ATTEND_TRUE
            for comment in Comment.objects.filter(relate_item=activity):
                student_data = {
                    "student_id": comment.student.username,
                    "student_username": comment.student.first_name,
                    "student_profile_url": comment.student.profile.post_url if comment.student.profile else None,
                }
                comment_data = {
                    "comment_id": comment.id,
                    "text": comment.text,
                    "student": student_data,
                }
                comments_data.append(comment_data)
            activity_data = {
                "activity_id": activity.id,
                "club_id": activity.activity_club.id,
                "image_url": activity.profile.post_url,
                "club_name": activity.activity_club.name,
                "name": activity.name,
                "content": activity.content,
                "category": activity.activity_club.category,
                "area": activity.area,
                "start_time": activity.start_time.strftime('%Y年%m月%d日 %H:%M:%S'),
                "end_time": activity.end_time.strftime('%Y年%m月%d日 %H:%M:%S'),
                "attend": attend,
                "comments": comments_data,
            }
            return Response({"status": 200, "data": activity_data})
        except Activity.DoesNotExist:
            return Response({"status": 400, "msg": "活动不存在"})  # 活动不存在
        except Exception as e:
            return Response({"status": 500, "msg": f"发生错误：{str(e)}"})  # 服务器错误

