from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

ID_LEN = 10
NAME_LEN = 30
URL_LEN = 500
TEXT_LEN = 1000


class File(models.Model):
    local_dir = models.CharField(max_length=URL_LEN)
    post_url = models.CharField(max_length=URL_LEN)


class Item(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=NAME_LEN)
    profile = models.ForeignKey(File, on_delete=models.CASCADE)
    star_comment = models.ForeignKey('Comment', null=True, on_delete=models.SET_NULL)  # Featured comment
    average_score = models.FloatField(null=True)

    class Meta:
        db_table = 'item'
        verbose_name = 'Item'
        verbose_name_plural = 'Items'


class Club(Item):
    star = models.IntegerField()
    category = models.CharField(max_length=NAME_LEN)
    introduction = models.CharField(max_length=TEXT_LEN)

    class Meta:
        db_table = 'club'
        verbose_name = 'Club'
        verbose_name_plural = 'Clubs'


class Activity(Item):
    activity_club = models.ForeignKey(Club, on_delete=models.CASCADE)
    content = models.CharField(max_length=TEXT_LEN)
    area = models.CharField(max_length=TEXT_LEN)
    maxNum = models.IntegerField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    class Meta:
        db_table = 'activity'
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'


class Push(Item):
    push_club = models.ForeignKey(Club, on_delete=models.CASCADE)
    pub_time = models.DateTimeField()
    url = models.CharField(max_length=URL_LEN)
    text = models.CharField(max_length=TEXT_LEN)


    class Meta:
        db_table = 'push'
        verbose_name = 'Push'
        verbose_name_plural = 'Pushes'


class Student(User):
    major = models.CharField(max_length=NAME_LEN, db_column='student_major')
    profile = models.ForeignKey(File, on_delete=models.CASCADE)
    club = models.ForeignKey(Club, on_delete=models.SET_NULL, null=True)
    is_chairman = models.BooleanField(db_column='is_chairman')

    class Meta:
        db_table = 'student'
        verbose_name = 'Student'
        verbose_name_plural = 'Students'

    def __str__(self):
        return self.username


class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.CharField(max_length=TEXT_LEN, null=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    relate_item = models.ForeignKey(Item, on_delete=models.CASCADE)

    class Meta:
        db_table = 'comment'
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'


class Score(models.Model):
    id = models.AutoField(primary_key=True)
    value = models.IntegerField()
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    relate_item = models.ForeignKey(Item, on_delete=models.CASCADE)

    class Meta:
        db_table = 'score'
        verbose_name = 'Score'
        verbose_name_plural = 'Scores'
        unique_together = ('student', 'relate_item')


class StudentRequest(models.Model):
    id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=NAME_LEN)
    pub_date = models.DateField(auto_now_add=now)

    class Meta:
        db_table = 'student_request'
        verbose_name = 'Student Request'
        verbose_name_plural = 'Student Requests'


class Bill(StudentRequest):
    money = models.FloatField()
    text = models.CharField(max_length=TEXT_LEN)
    club = models.ForeignKey(Club, on_delete=models.CASCADE)
    file = models.ForeignKey(File, on_delete=models.CASCADE)

    class Meta:
        db_table = 'bill'
        verbose_name = 'Bill'
        verbose_name_plural = 'Bills'


class StudentCreateClub(StudentRequest):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    name = models.CharField(max_length=NAME_LEN)
    category = models.CharField(max_length=NAME_LEN)
    introduction = models.CharField(max_length=TEXT_LEN)
    profile = models.ForeignKey(File, on_delete=models.CASCADE)

    class Meta:
        db_table = 'student_create_club'
        verbose_name = 'Student Create Club Request'
        verbose_name_plural = 'Student Create Club Requests'


class StudentCreateActivity(StudentRequest):
    activity_club = models.ForeignKey(Club, on_delete=models.CASCADE)
    content = models.CharField(max_length=TEXT_LEN)
    area = models.CharField(max_length=TEXT_LEN)
    maxNum = models.IntegerField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    class Meta:
        db_table = 'student_create_activity'
        verbose_name = 'Student Attend Activity Request'
        verbose_name_plural = 'Student Attend Activity Requests'


class StudentAttendClub(StudentRequest):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    club = models.ForeignKey(Club, on_delete=models.CASCADE)

    class Meta:
        db_table = 'student_attend_club'
        verbose_name = 'Student Attend Club Request'
        verbose_name_plural = 'Student Attend Club Requests'


class StudentAttendActivity(StudentRequest):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)

    class Meta:
        db_table = 'student_attend_activity'
        verbose_name = 'Student Attend Activity Request'
        verbose_name_plural = 'Student Attend Activity Requests'



